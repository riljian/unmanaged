from django.db import models


class DeviceGroup(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255)

    class Meta:
        managed = False


class Device(models.Model):
    device_id = models.CharField(primary_key=True, max_length=255)
    groups = models.ManyToManyField(
        DeviceGroup, related_name='containing_devices', blank=True)

    class Meta:
        managed = False
